package monoxide.forgebackup.coremod.asm;

import monoxide.core.asm.BasicTransformer;

import monoxide.core.logging.MonoLog;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

public class EssentialsBackupTransformer extends BasicTransformer {
	public EssentialsBackupTransformer() {
		this.logger = new MonoLog("forgebackup");
		addClass("com.ForgeEssentials.backup.ModuleBackup");
	}
	
	@Override
	public void transform(ClassNode classNode) {
		logger.info("Mangling ForgeEssentials for its own good. Their Backups module was not actually loaded.");
		
		classNode.fields.clear();
		for (int i = 0; i < classNode.methods.size();) {
			MethodNode node = (MethodNode)classNode.methods.get(i);

			logger.fine("Inspecting method: %s", node.name);
			if (!node.name.equals("<init>")) {
				classNode.methods.remove(i);
			} else {
				i++;
			}
		}
	}
}
